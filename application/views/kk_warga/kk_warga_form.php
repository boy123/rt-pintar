
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
            </div>
        <div class="widget-body">
        <div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Wilayah <?php echo form_error('id_wilayah') ?></label>
            <select class="form-control" name="id_wilayah" required>
                <option value="">Pilih Wilayah</option>
                <?php foreach ($this->db->get('wilayah')->result() as $rw): ?>
                    <?php if ($rw->id == $id_wilayah): ?>
                        <option value="<?php echo $rw->id ?>" selected><?php echo $rw->wilayah ?></option>
                    <?php endif ?>
                    <option value="<?php echo $rw->id ?>"><?php echo $rw->wilayah ?></option>
                <?php endforeach ?>

            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Kepala Kk <?php echo form_error('kepala_kk') ?></label>
            <input type="text" class="form-control" name="kepala_kk" id="kepala_kk" placeholder="Kepala Kk" value="<?php echo $kepala_kk; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Bulan Daftar <?php echo form_error('bulan_daftar') ?></label>
            <select name="bulan_daftar" id="bulan_daftar" style="width:100%;" required>
                <option value="">--Pilih Bulan --</option>
                <?php 
                for ($i=1; $i <= 12; $i++) { 
                    if ($i == $bulan_daftar) {
                        $checked = 'selected';
                    } else {
                        $checked = '';
                    }
                    ?>
                    <option value="<?php echo $i ?>" <?php echo $checked ?>><?php echo bulan_indo($i) ?></option>
                    <?php
                }
                 ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="year">Tahun Daftar <?php echo form_error('tahun_daftar') ?></label>
            <select name="tahun_daftar" id="tahun_daftar" style="width:100%;" required>
                <option value="">--Pilih Tahun --</option>
                <?php 
                for ($i=24; $i <= 29; $i++) { 
                    if ($i == substr($tahun_daftar, 2,2)) {
                        $checked = 'selected';
                    } else {
                        $checked = '';
                    }
                    $tahun = '20'.$i;
                    ?>
                    <option value="<?php echo $tahun ?>" <?php echo $checked ?>><?php echo $tahun ?></option>
                    <?php
                }
                 ?>
            </select>
        </div>
	    <input type="hidden" name="id_kk" value="<?php echo $id_kk; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('kk_warga') ?>" class="btn btn-default">Cancel</a>
	</form>
                                    </div>
                                </div>
                                </div>
   