<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Filter</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="" method="get" role="form">
                    <!-- <div class="form-group">
                        <select name="id_wilayah" id="id_wilayah" style="width:100%;" required="">
			                <option value="">--Pilih Wilayah --</option>
			                <?php 
			                foreach ($this->db->get('wilayah')->result() as $rw): 
			                	if ($id_wilayah == $rw->id) {
			                		$checked = 'selected';
			                	} else {
			                		$checked = '';
			                	}
			                    ?>
			                    <option value="<?php echo $rw->id ?>" <?php echo $checked ?>><?php echo $rw->wilayah ?></option>
			                <?php endforeach ?>
			            </select>
                    </div> -->

                    <div class="form-group">
                    	<select name="id_kk" id="id_kk" style="width:100%;" required="">
			                <option value="">--Pilih Kepala Keluarga --</option>
			                <?php 
			                if ($this->session->userdata('level') == 3) {
					            $this->db->where('created_user', $this->session->userdata('id_user'));
					        }
			                foreach ($this->db->get('kk_warga')->result() as $rw): 
			                	if ($id_kk == $rw->id_kk) {
			                		$checked = 'selected';
			                	} else {
			                		$checked = '';
			                	}
			                    ?>
			                    <option value="<?php echo $rw->id_kk ?>" <?php echo $checked ?>><?php echo strtoupper($rw->kepala_kk) ?></option>
			                <?php endforeach ?>
			            </select>
                    </div>
                    <div class="form-group">
                    	<select name="tahun" id="tahun" style="width:100%;" required="">
			                <option value="">--Pilih Tahun --</option>
			                <?php 
			                $end_tahun = 2030;
						    $tahun_mulai = 2024;
						    
						    for ($tahun = $tahun_mulai; $tahun <= $end_tahun; $tahun++) {
						        

				                if ($tahun == $tahun_select) {
			                		$checked = 'selected';
			                	} else {
			                		$checked = '';
			                	}
			                	
			                    ?>
			                    <option value="<?php echo $tahun ?>" <?php echo $checked ?>><?php echo $tahun ?></option>
			                <?php } ?>
			            </select>
                    </div>

                    
                    <button type="submit" class="btn btn-primary">CARI</button>

                </form>
            </div>
        </div>
    </div>
</div>
<?php if (isset($_GET['id_kk'])): 
	$this->db->where('id_kk', $id_kk);
	$rw = $this->db->get('kk_warga')->row();
	?>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Data Warga</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<table class="table table-hover table-striped table-bordered">
        			<tr>
        				<td>Nama</td>
        				<td><b><?php echo strtoupper(get_data('kk_warga','id_kk',$id_kk,'kepala_kk')) ?></b></td>
        			</tr>
        			<tr>
        				<td>Wilayah</td>
        				<td><b><?php echo get_data('wilayah','id',$rw->id_wilayah,'wilayah') ?></b></td>
        			</tr>
        			<tr>
        				<td>Tahun Pembayaran</td>
        				<td><b><?php echo $tahun_select ?></b></td>
        			</tr>

        		</table>

            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">BULAN PEMBAYARAN</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<table class="table table-hover table-striped table-bordered">
            		<tr>
            			<th>BULAN</th>
            			<td align="center"><b>TGL BAYAR</b></td>
            			<td align="center"><b>AKSI</b></td>
            		</tr>
            		<?php 
            		if ($rw->tahun_daftar == $tahun_select) {
            			$bln = $rw->bulan_daftar;
            		} else {
            			$bln = 1;
            		}
            		for ($i=$bln; $i <= 12; $i++) { 
            			$bulan = "";
            			$tahun = "";
            			$tgl_bayar = "";
            			$lunas = 0;

            			$this->db->where('id_kk', $id_kk);
            			$this->db->where('bulan', $i);
            			$this->db->where('tahun', $tahun_select);
            			$dataIuran = $this->db->get('iuran_kematian');
            			if ($dataIuran->num_rows() == 1) {
            				$data = $dataIuran->row();

            				$bulan = $data->bulan;
            				$tahun = $data->tahun;
            				$tgl_bayar = $data->tgl_bayar;
            				$lunas = $data->lunas;
            			}
            		 ?>
        			<tr>
        				<td><?php echo strtoupper(bulan_indo($i)) ?></td>
        				<td align="center">
        					<?php echo ($lunas == 0) ? '' : date('d-m-Y', strtotime($tgl_bayar)) ; ?>
        				</td>
        				<td align="center">
							<?php if ($this->session->userdata('level') == 3): ?>
								<?php if ($lunas == 0): ?>
									<a href="app/lunas_iuran_kematian?<?php echo param_get() ?>&bulan=<?php echo $i ?>&tahun=<?php echo $tahun_select ?>" class="btn btn-primary">DIBAYAR</a>
								<?php else: ?>
									<!-- jika masih di bulan saat ini -->
									<?php if (strtotime($tgl_bayar) == strtotime(date('Y-m-d'))): ?>
										<a href="app/batal_iuran_kematian/<?php echo $data->id ?>?<?php echo param_get() ?>" class="btn btn-danger" onclick="javasciprt: return confirm('Yakin akan batalkan pembayaran dari <?php echo strtoupper(get_data('kk_warga','id_kk',$id_kk,'kepala_kk')) ?> ?')">BATALKAN</a>
									<!-- jika di bulan lain -->
									<?php else: ?>
										<b style="color: green;">LUNAS</b>
									<?php endif ?>
									
								<?php endif ?>
							<?php else: ?>
								<?php if ($lunas == 0): ?>
									<a href="app/lunas_iuran_kematian?<?php echo param_get() ?>&bulan=<?php echo $i ?>&tahun=<?php echo $tahun_select ?>" class="btn btn-primary">DIBAYAR</a>
								<?php else: ?>
									<a href="app/batal_iuran_kematian/<?php echo $data->id ?>?<?php echo param_get() ?>" class="btn btn-danger" onclick="javasciprt: return confirm('Yakin akan batalkan pembayaran dari <?php echo strtoupper(get_data('kk_warga','id_kk',$id_kk,'kepala_kk')) ?> ?')">BATALKAN</a>
									
								<?php endif ?>
							<?php endif ?>
        				</td>
        			</tr>
        			<?php
        			}
        			
        			 ?>

        		</table>

            </div>
        </div>
    </div>
</div>

<?php endif ?>

<!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.7.14"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script> -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"><?php echo $this->session->userdata('message') ?></script>
<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    // $("#id_wilayah").select2();
	    $("#id_kk").select2();
	});

	</script>
</script>