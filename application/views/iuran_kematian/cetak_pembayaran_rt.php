<center>
	<h2>Rekap Pembayaran</h2>
</center>

<?php 
$total_kas = 0;
$total_disetor = $this->db->query("select sum(jumlah) AS jumlah from setoran order by id DESC")->row()->jumlah;
$total_belum_disetor = 0;
foreach ($this->db->get('wilayah')->result() as $wilayah): ?>
	

<div class="row">
	<div class="col-md-12">
		<h3><?php echo $wilayah->wilayah ?></h3>
	</div>
	<div class="col-md-12">
		<table class="table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama</th>
					<th>Daftar</th>
					<th>Bulan terbayar</th>
					<th>Pendaftaran</th>
					<th>Iuran</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$subtotal = 0;
				$subtotalbulan = 0;
				$no = 1;
				$sql = "SELECT * FROM kk_warga WHERE id_wilayah = $wilayah->id ";
				foreach ($this->db->query($sql)->result() as $rw): 

				$sql_total_bulan = "SELECT id FROM iuran_kematian WHERE id_kk='$rw->id_kk' and lunas=1 ";

				$bulan_terbayar = $this->db->query($sql_total_bulan)->num_rows();
				$uang_pendaftaran = 0;
				if ($bulan_terbayar > 0) {
					$uang_pendaftaran = 50000;
				}
				$uang_iuran = $bulan_terbayar * 4000;
				$total_uang_warga = $uang_pendaftaran + $uang_iuran;
				$subtotal = $subtotal + $total_uang_warga;
				$total_kas = $total_kas + $total_uang_warga;
				$subtotalbulan = $subtotalbulan + $bulan_terbayar

					?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->kepala_kk ?></td>
					<td><?php echo bulan_indo($rw->bulan_daftar).' '.$rw->tahun_daftar ?></td>
					<td><?php echo $bulan_terbayar ?></td>
					<td align="right">Rp. <?php echo number_format($uang_pendaftaran, 0, ',', '.') ?></td>
					<td align="right">Rp. <?php echo number_format($uang_iuran, 0, ',', '.') ?></td>
					<td align="right">Rp. <?php echo number_format($total_uang_warga, 0, ',', '.') ?></td>
				</tr>
				<?php $no++; endforeach ?>
				<tr>
					<td>Subtotal</td>
					<td colspan="5" align="right"><b><?php echo $subtotalbulan ?></b></td>
					<td  align="right"><b>Rp. <?php echo number_format($subtotal, 0, ',', '.') ?></b></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<?php endforeach ?>

<?php 
$total_belum_disetor = $total_kas - $total_disetor;
 ?>
<div class="row">
	<div class="col-md-12">
		<h3>Keterangan</h3>
	</div>
	<div class="col-md-12">
		<table class="table table-bordered">
			<tr>
				<th>Total Kas Kita</th>
				<th align="right">Rp. <?php echo number_format($total_kas, 0, ',', '.') ?></th>
			</tr>
			<tr>
				<th>Total Telah disetor (KE RT)</th>
				<th align="right">Rp. <?php echo number_format($total_disetor, 0, ',', '.') ?></th>
			</tr>
			<tr>
				<th>Total Belum disetor (KE RT)</th>
				<th align="right">Rp. <?php echo number_format($total_belum_disetor, 0, ',', '.') ?></th>
			</tr>
		</table>
	</div>
</div>