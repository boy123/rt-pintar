<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success" style="background-color: #0c15bd">
			<h2>Selamat datang, <?php echo $this->session->userdata('nama'); ?></h2>
		</div>
		
	</div>

</div>

<div class="row">
	<div class="col-md-6">
		<div class="alert alert-success">
			<h4>TOTAL BAYAR BULAN INI</h4>
			<h1>Rp <?php echo number_format($total_sudah_bayar,2) ?></h1>
		</div>
	</div>
	<div class="col-md-6">
		<a href="app?belum_bayar=1">
			<div class="alert alert-danger">
				<h4>BELUM BAYAR BULAN INI</h4>
				<h1>Rp <?php echo number_format($total_belum_bayar,2) ?></h1>
			</div>
		</a>
	</div>

	<?php if (isset($_GET['belum_bayar'])) :?>
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">DAFTAR BELUM BAYAR</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<table class="table table-hover table-striped table-bordered">
            		<tr>
						<th>No.</th>
						<th>Nama</th>
						<th>Wilayah</th>
					</tr>
					<?php 
					$no = 1;
					foreach ($listBelumBayar as $rw) { ?>
					<tr>
						<td><?php echo $no ?></td>
						<td><?php echo $rw->kepala_kk ?></td>
						<td><?php echo get_data('wilayah','id',$rw->id_wilayah,'wilayah') ?></td>
					</tr>
					<?php $no++; } ?>

        		</table>

            </div>
        </div>
    </div>
	<?php endif ?>

</div>
