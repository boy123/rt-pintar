<div class="page-sidebar" id="sidebar">
    <!-- Page Sidebar Header-->
    <div class="sidebar-header-wrapper">
        <form>
            <input type="text" name="menu" class="searchinput" autocomplete="off" />
            <i class="searchicon fa fa-search"></i>
            <div class="searchhelper">Cari menu </div>
        </form>
        
    </div>
    <!-- /Page Sidebar Header -->
    <!-- Sidebar Menu -->
    <ul class="nav sidebar-menu">
        <?php if ($this->session->userdata('level') == '1' || $this->session->userdata('level') == '2'): ?>
        <li>
            <a href="app">
                <i class="menu-icon fa fa-desktop"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>
        <li>
            <a href="wilayah">
                <i class="menu-icon fa fa-server"></i>
                <span class="menu-text"> Wilayah </span>
            </a>
        </li>
        <li>
            <a href="kk_warga">
                <i class="menu-icon fa fa-user-plus"></i>
                <span class="menu-text"> Data Warga </span>
            </a>
        </li>
        <li>
            <a href="app/iuran_kematian">
                <i class="menu-icon fa fa-shopping-cart"></i>
                <span class="menu-text"> Iuran Kematian </span>
            </a>
        </li>
        <li>
            <a href="app/cetak_pembayaran">
                <i class="menu-icon fa fa-print"></i>
                <span class="menu-text"> Rekap Pembayaran </span>
            </a>
        </li>
        <li>
            <a href="level">
                <i class="menu-icon fa fa-line-chart"></i>
                <span class="menu-text"> Level </span>
            </a>
        </li>
        <li>
            <a href="users">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> Data User </span>
            </a>
        </li>
        <?php endif ?>

        <?php if ($this->session->userdata('level') == '3'): ?>
        <li>
            <a href="app">
                <i class="menu-icon fa fa-desktop"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>
        <li>
            <a href="kk_warga">
                <i class="menu-icon fa fa-user-plus"></i>
                <span class="menu-text"> Data Warga </span>
            </a>
        </li>
        <li>
            <a href="app/iuran_kematian">
                <i class="menu-icon fa fa-shopping-cart"></i>
                <span class="menu-text"> Iuran Kematian </span>
            </a>
        </li>
        <li>
            <a href="app/cetak_pembayaran">
                <i class="menu-icon fa fa-print"></i>
                <span class="menu-text"> Rekap Pembayaran </span>
            </a>
        </li>
        
        <?php endif ?>
        
    </ul>
    <!-- /Sidebar Menu -->
</div>