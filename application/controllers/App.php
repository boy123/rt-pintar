<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('level') == '') {
			redirect('login','refresh');
		}

		if ($this->session->userdata('level') == 3) {
			//total bayar di bulan ini
			$bulanIni = substr(date('m'), 1,2);
			$tahunIni = date('Y');
			$id_user = $this->session->userdata('id_user');
			$queryBayar = "SELECT COUNT(a.id_kk) as total FROM kk_warga a INNER JOIN iuran_kematian b ON a.id_kk = b.id_kk WHERE b.bulan=$bulanIni AND b.tahun=$tahunIni AND b.lunas=1 AND a.created_user=$id_user";

			$queryBelumBayar = "SELECT COUNT(a.id_kk) as total FROM kk_warga a WHERE a.created_user=$id_user";

			$queryListBelumBayar = "SELECT * FROM kk_warga WHERE id_kk NOT IN (SELECT a.id_kk FROM kk_warga a INNER JOIN iuran_kematian b ON a.id_kk = b.id_kk WHERE b.bulan=$bulanIni AND b.tahun=$tahunIni AND b.lunas=1 AND a.created_user=$id_user) AND created_user=$id_user ORDER BY id_wilayah ASC";

			$total_sudah_bayar = $this->db->query($queryBayar)->row()->total * 4000;
			$sementara_belum_bayar = $this->db->query($queryBelumBayar)->row()->total * 4000;

			$data = array(
				'konten' => 'home',
				'judul_page' => 'Dashboard',
				'total_sudah_bayar' => $total_sudah_bayar,
				'total_belum_bayar' => $sementara_belum_bayar - $total_sudah_bayar,
				'listBelumBayar' =>$this->db->query($queryListBelumBayar)->result(),
			);
			$this->load->view('v_index',$data);
		} else {
			//total bayar di bulan ini
			$bulanIni = substr(date('m'), 1,2);
			$tahunIni = date('Y');
			$queryBayar = "SELECT COUNT(a.id_kk) as total FROM kk_warga a INNER JOIN iuran_kematian b ON a.id_kk = b.id_kk WHERE b.bulan=$bulanIni AND b.tahun=$tahunIni AND b.lunas=1";

			$queryBelumBayar = "SELECT COUNT(a.id_kk) as total FROM kk_warga a";

			$queryListBelumBayar = "SELECT * FROM kk_warga WHERE id_kk NOT IN (SELECT a.id_kk FROM kk_warga a INNER JOIN iuran_kematian b ON a.id_kk = b.id_kk WHERE b.bulan=$bulanIni AND b.tahun=$tahunIni AND b.lunas=1) ORDER BY id_wilayah ASC";

			$total_sudah_bayar = $this->db->query($queryBayar)->row()->total * 4000;
			$sementara_belum_bayar = $this->db->query($queryBelumBayar)->row()->total * 4000;

			$data = array(
				'konten' => 'home',
				'judul_page' => 'Dashboard',
				'total_sudah_bayar' => $total_sudah_bayar,
				'total_belum_bayar' => $sementara_belum_bayar - $total_sudah_bayar,
				'listBelumBayar' =>$this->db->query($queryListBelumBayar)->result(),
			);
			$this->load->view('v_index',$data);
		}


	}

	public function iuran_kematian()
	{
		if ($this->session->userdata('level') == '') {
			redirect('login','refresh');
		}
		if (isset($_GET['id_kk'])) {
			// $id_wilayah = $this->input->get('id_wilayah');
			$id_kk = $this->input->get('id_kk');
			$tahun = $this->input->get('tahun');
		} else {
			// $id_wilayah = '';
			$id_kk = '';
			$tahun = '';
		}
		$data = array(
			'konten' => 'iuran_kematian/view',
			'judul_page' => 'Iuran Kematian',
			// 'id_wilayah' => $id_wilayah,
			'id_kk' => $id_kk,
			'tahun_select' => $tahun
		);
		$this->load->view('v_index',$data);
	}

	public function cetak_pembayaran()
	{
		$data = array(
			'konten' => 'iuran_kematian/cetak_pembayaran',
			'judul_page' => 'Cetak Pembayaran',
		);
		$this->load->view('v_index',$data);
	}

	public function cetak_pembayaran_rt()
	{
		$data = array(
			'konten' => 'iuran_kematian/cetak_pembayaran_rt',
			'judul_page' => 'Cetak Pembayaran',
		);
		$this->load->view('v_index',$data);
	}

	public function cetak_pembayaran_wilayah()
	{
		$data = array(
			'konten' => 'iuran_kematian/pembayaran_wilayah',
			'judul_page' => 'Cetak Pembayaran',
		);
		$this->load->view('v_index',$data);
	}


	public function lunas_iuran_kematian()
	{
		if (isset($_GET['id_kk'])) {
			$id_kk = $this->input->get('id_kk');
			$bulan = $this->input->get('bulan');
			$tahun = $this->input->get('tahun');

			$this->db->where('id_kk', $id_kk);
			$this->db->where('bulan', $bulan);
			$this->db->where('tahun', $tahun);
			$iuran = $this->db->get('iuran_kematian');
			if ($iuran->num_rows() > 0) {
				$rw = $iuran->row();
				//update data
				$dataUpdated = array(
					"lunas" => 1,
					"tgl_bayar" => date('Y-m-d'),
					"updated_at" => get_waktu(),
					"updated_user" => $this->session->userdata('id_user'),
				);
				$this->db->where('id', $rw->id);
				$this->db->update('iuran_kematian', $dataUpdated);
			} else {
				//insert data
				$dataInsert = array(
					"id_kk" => $id_kk,
					"bulan" => $bulan,
					"tahun" => $tahun,
					"lunas" => 1,
					"tgl_bayar" => date('Y-m-d'),
					"created_at" => get_waktu(),
					"created_user" => $this->session->userdata('id_user'),
				);
				$this->db->insert('iuran_kematian', $dataInsert);
			}
			$this->session->set_flashdata('message', alert_biasa('Sukses Melakukan Pembayaran','success'));
			redirect('app/iuran_kematian?'.param_get(),'refresh');
		} else {
			redirect('app','refresh');
		}
	}

	public function batal_iuran_kematian($id)
	{
		$user_at = $this->session->userdata('id_user');
		$sqlUpdated = "UPDATE iuran_kematian SET lunas=0, tgl_bayar=null, updated_at=now(), updated_user=$user_at WHERE id=$id ";
		$this->db->query($sqlUpdated);
		$this->session->set_flashdata('message', alert_biasa('Sukses Batal Pembayaran','warning'));
		redirect('app/iuran_kematian?'.param_get(),'refresh');
	}

}

/* End of file App.php */
/* Location: ./application/controllers/App.php */