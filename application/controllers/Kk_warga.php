<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kk_warga extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kk_warga_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'kk_warga/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'kk_warga/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'kk_warga/index.html';
            $config['first_url'] = base_url() . 'kk_warga/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Kk_warga_model->total_rows($q);
        $kk_warga = $this->Kk_warga_model->get_limit_data($config['per_page'], $start, $q);
        //log_r($this->db->last_query());

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'kk_warga_data' => $kk_warga,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Warga',
            'konten' => 'kk_warga/kk_warga_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Kk_warga_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_kk' => $row->id_kk,
		'id_wilayah' => $row->id_wilayah,
		'kepala_kk' => $row->kepala_kk,
		'bulan_daftar' => $row->bulan_daftar,
		'tahun_daftar' => $row->tahun_daftar,
		'created_at' => $row->created_at,
		'created_user' => $row->created_user,
		'updated_at' => $row->updated_at,
		'updated_user' => $row->updated_user,
	    );
            $this->load->view('kk_warga/kk_warga_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kk_warga'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Data Warga',
            'konten' => 'kk_warga/kk_warga_form',
            'button' => 'Simpan',
            'action' => site_url('kk_warga/create_action'),
	    'id_kk' => set_value('id_kk'),
	    'id_wilayah' => set_value('id_wilayah'),
	    'kepala_kk' => set_value('kepala_kk'),
	    'bulan_daftar' => set_value('bulan_daftar'),
	    'tahun_daftar' => set_value('tahun_daftar'),
	    'created_at' => set_value('created_at'),
	    'created_user' => set_value('created_user'),
	    'updated_at' => set_value('updated_at'),
	    'updated_user' => set_value('updated_user'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_wilayah' => $this->input->post('id_wilayah',TRUE),
		'kepala_kk' => $this->input->post('kepala_kk',TRUE),
		'bulan_daftar' => $this->input->post('bulan_daftar',TRUE),
		'tahun_daftar' => $this->input->post('tahun_daftar',TRUE),
		'created_at' => get_waktu(),
		'created_user' => $this->session->userdata('id_user'),
		'updated_at' => get_waktu(),
		'updated_user' => $this->session->userdata('id_user'),
	    );

            $this->Kk_warga_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan
                                    </div>');
            redirect(site_url('kk_warga'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Kk_warga_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Data Warga',
                'konten' => 'kk_warga/kk_warga_form',
                'button' => 'Ubah',
                'action' => site_url('kk_warga/update_action'),
		'id_kk' => set_value('id_kk', $row->id_kk),
		'id_wilayah' => set_value('id_wilayah', $row->id_wilayah),
		'kepala_kk' => set_value('kepala_kk', $row->kepala_kk),
		'bulan_daftar' => set_value('bulan_daftar', $row->bulan_daftar),
		'tahun_daftar' => set_value('tahun_daftar', $row->tahun_daftar),
		'created_at' => set_value('created_at', $row->created_at),
		'created_user' => set_value('created_user', $row->created_user),
		'updated_at' => set_value('updated_at', $row->updated_at),
		'updated_user' => set_value('updated_user', $row->updated_user),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kk_warga'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kk', TRUE));
        } else {
            $data = array(
		'id_wilayah' => $this->input->post('id_wilayah',TRUE),
		'kepala_kk' => $this->input->post('kepala_kk',TRUE),
		'bulan_daftar' => $this->input->post('bulan_daftar',TRUE),
		'tahun_daftar' => $this->input->post('tahun_daftar',TRUE),
		'updated_at' => get_waktu(),
		'updated_user' => $this->session->userdata('id_user'),
	    );

            $this->Kk_warga_model->update($this->input->post('id_kk', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('kk_warga'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Kk_warga_model->get_by_id($id);

        if ($row) {
            $this->Kk_warga_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('kk_warga'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kk_warga'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_wilayah', 'id wilayah', 'trim|required');
	$this->form_validation->set_rules('kepala_kk', 'kepala kk', 'trim|required');
	$this->form_validation->set_rules('bulan_daftar', 'bulan daftar', 'trim|required');
	$this->form_validation->set_rules('tahun_daftar', 'tahun daftar', 'trim|required');

	$this->form_validation->set_rules('id_kk', 'id_kk', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Kk_warga.php */
/* Location: ./application/controllers/Kk_warga.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2024-06-12 13:19:38 */
/* https://jualkoding.com */