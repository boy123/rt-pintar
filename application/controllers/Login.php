<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{
		if ($this->session->userdata('level') != '' && $this->session->userdata('id_user') != '') {
			redirect('app/index','refresh');
		}
		$data['judul_page'] = 'Login e-Arsip';
		$this->load->view('login',$data);
	}

	public function auth()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		if ($_POST) {
			
			$this->db->where('username', $username);
			$cek = $this->db->get('users');
			if ($cek->num_rows() == 0) {
				$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n username tidak ditemukan','warning'));
				redirect('login','refresh');
			} else {
				$users = $cek->row();
				if ($password == $users->password) {
					// jika berhasil login
					$sess_data['id_user'] = $users->id_user;
					$sess_data['nama'] = $users->nama_lengkap;
					$sess_data['username'] = $users->username;
					$sess_data['level'] = $users->id_level;
					$this->session->set_userdata($sess_data);

					redirect('app','refresh');

				} else {
					$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n password kamu salah','warning'));
					redirect('login','refresh');
				}
			}
			
		}
	}


	function logout()
	{

		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('login','refresh');
	}

}